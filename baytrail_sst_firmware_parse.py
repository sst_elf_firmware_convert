#!/usr/bin/env python
#
# baytrail_sst_firmware_parse.py - parse an SST firmware file
#
# Copyright (C) 2015  Antonio Ospite <ao2@ao2.it>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.

import os
import sys
import struct

FIRMWARE_HEADER_SIZE = 32
MODULE_HEADER_SIZE = 20


def get(f, fmt, offset=None):
    if offset:
        f.seek(offset)

    length = struct.calcsize(fmt)
    data = f.read(length)
    value = struct.unpack_from(fmt, data)
    return value[0]


def sst_firmware_parse(firmware_file_name):

    f = open(firmware_file_name, "rb")
    filesize = os.path.getsize(firmware_file_name)

    signature = get(f, "4s")
    if signature != "$SST":
        print "Invalid file"
        return False

    payload_size = get(f, '<L')
    print "Payload size:", payload_size

    if (payload_size + FIRMWARE_HEADER_SIZE) != filesize:
        print "invalid payload size"
        return False

    num_modules = get(f, '<L')
    print "Number of modules:", num_modules

    file_format = get(f, '<L')
    print "File format:", file_format

    # reserved fields
    get(f, '<L')
    get(f, '<L')
    get(f, '<L')
    get(f, '<L')

    for i in range(0, num_modules):
        print "\nModule %d" % i

        signature = get(f, "4s")
        if signature != "$SST":
            print "Invalid module"
            return False

        module_size = get(f, '<L')
        print "module size:", module_size

        if (module_size + MODULE_HEADER_SIZE) != payload_size:
            print "Invalid module size:"
            return False

        num_blocks = get(f, '<L')
        print "Number of blocks:", num_blocks

        block_type = get(f, '<L')
        print "Block type: ", block_type

        entry_point = get(f, '<L')
        print "Entry point: 0x%08x" % entry_point

        total_block_size = 0
        for j in range(0, num_blocks):
            print "\nBlock %d" % j

            ram_type = get(f, '<L')
            print "Ram type:", ram_type

            block_size = get(f, '<L')
            print "Block size: %d (0x%08x)" % (block_size, block_size)

            total_block_size += block_size + 16
            print "Total block size: %d (0x%08x)" % \
                (total_block_size, total_block_size)

            ram_offset = get(f, '<L')
            print "Ram offset: 0x%08x" % ram_offset

            # reserved
            get(f, '<L')

            # actual data
            get(f, "%db" % block_size)

        if total_block_size != module_size:
            print "Invalid total block size:"
            return False

    return True


if __name__ == "__main__":
    filename = sys.argv[1]
    ret = sst_firmware_parse(filename)

    sys.exit(ret)
